<!doctype html>
<html lang="en">

<head>
  <title>Filter reviews</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
  <div class="container">
    <div class="row">
      <div class="col-md-4 offset-4">
        <h4 class="mt-5 text-center mb-3">Filter reviews</h4>
        <form action="index.php" method="GET">
          <div class="form-group">
            <label for="rating">Order by rating:</label>
            <select class="form-control" name="rating" id="rating">
              <option value="highest" <?php if(isset($_GET['rating']) == 'highest') echo "selected"; ?>>Highest First</option>
              <option value="lowest" <?php if(isset($_GET['rating']) == 'lowest')  echo "selected"; ?>>Lowest First</option>
            </select>
          </div>

          <div class="form-group">
            <label for="minimumRating">Minimum rating:</label>
            <select class="form-control" name="minimumRating" id="minimumRating">
              <option value="1" <?php if(isset($_GET['minimumRating']) == 1) echo "selected"; ?>>1</option>
              <option value="2" <?php if(isset($_GET['minimumRating']) == 2) echo "selected"; ?>>2</option>
              <option value="3" <?php if(isset($_GET['minimumRating']) == 3) echo "selected"; ?>>3</option>
              <option value="4" <?php if(isset($_GET['minimumRating']) == 4) echo "selected"; ?>>4</option>
              <option value="5" <?php if(isset($_GET['minimumRating']) == 5) echo "selected"; ?>>5</option>
            </select>
          </div>

          <div class="form-group">
            <label for="date">Order by date:</label>
            <select class="form-control" name="date" id="date">
              <option value="newest" <?php if(isset($_GET['date']) == 'newest') echo "selected"; ?>>Newest First</option>
              <option value="oldest" <?php if(isset($_GET['date']) == 'oldest') echo "selected"; ?>>Oldest First</option>
            </select>
          </div>

          <div class="form-group">
            <label for="text">Prioritize by text:</label>
            <select class="form-control" name="text" id="text">
              <option value="yes" <?php if(isset($_GET['text']) == 'yes') echo "selected"; ?>>Yes</option>
              <option value="no" <?php if(isset($_GET['text']) == 'no') echo "selected"; ?>>No</option>
            </select>
          </div>

          <button type="submit" class="btn btn-primary" name="submit" value="submited">Submit</button>
        </form>
      </div>
    </div>
  </div>

 <?php 
  if(isset($_GET['submit'])) {
    require_once('filter.php'); 
  }
  ?>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>