<?php
error_reporting(E_ALL & ~E_NOTICE);
error_reporting(E_ALL ^ E_WARNING); 
$json_url = "reviews.json";

$str = file_get_contents($json_url);
$json = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $str), true);


if (isset($_GET['rating']) && isset($_GET['minimumRating']) && isset($_GET['date']) && isset($_GET['text']) && isset($_GET['submit'])) {

    $rating = [];
    $date = [];
    $text = [];

    $minimumRating = $_GET['minimumRating'];
    for ($i = 0; $i < count($json); $i++) {
        if ($minimumRating <= $json[$i]['rating']) {
            $rating[] = $json[$i]['rating'];
            $date[] = $json[$i]['reviewCreatedOnDate'];
            $text[] = $json[$i]['reviewText'];
        } else {
            unset($json[$i]);
        }
    }

    #Default sorting
    $ratingSort = SORT_ASC;
    $dateSort = SORT_ASC;

    if ($_GET['rating'] == 'highest') {
        $ratingSort = SORT_DESC;
    }

    if ($_GET['date'] == 'newest') {
        $dateSort = SORT_DESC;
    }

    if ($_GET['text'] == 'yes') {
        array_multisort(
            $text,
            SORT_STRING,
            SORT_DESC,
            $rating,
            $ratingSort,
            SORT_NUMERIC,
            $date,
            $dateSort,
            SORT_STRING,
            $json
        );
    } else {
        array_multisort(
            $rating,
            $ratingSort,
            SORT_NUMERIC,
            $date,
            $dateSort,
            SORT_STRING,
            $json
        );
    }

    echo "
    <div class='container mt-5'>
        <div class='row'>
            <div class='col md-4'>
                <table class='table'>
                    <thead>
                    <tr>
                        <th scope='col'>id</th>
                        <th scope='col'>Review</th>
                        <th scope='col'>Rating</th>
                        <th scope='col'>Date</th>
                    </tr>
                    </thead>
                    <tbody>";
    for ($i = 0; $i < count($json); $i++) {
        if (isset($json[$i]['reviewCreatedOnDate'])) {
            echo "<tr>";
            echo "<td>" . $json[$i]['id'] . "</td>";
            echo "<td>" . $json[$i]['reviewText'] . "</td>";
            echo "<td>" . $json[$i]['rating'] . "</td>";
            echo "<td>" . date('Y-M-d H:i', strtotime($json[$i]['reviewCreatedOnDate'])) . "</td>";
            echo "</tr>";
        }
    }
    echo "</tbody>
                </table>
            </div>
        </div>
    </div>";
}
